# Charlie-Tools

A Repo with helper scripts for EasyAvi local debugging

## Installation

Clone the Repo

```bash
git clone https://gitlab.com/charliegut14/charlie-tools.git
```


## Usage

init.sh will build an EasyAvi container from your source code in ~/go/src/easyavi as well as a mongodb and mongo-express container. When ran a second time it will clean up the previous instance and redeploy

```bash
./init.sh

Cleaning Up Old Containers
Stopping easyavi       ... done
Stopping mongo_express ... done
Stopping mongo         ... done
Removing easyavi       ... done
Removing mongo_express ... done
Removing mongo         ... done
Removing network charlie-tools_default
Brining Up New Containers in Detached Mode
Creating network "charlie-tools_default" with the default driver
Creating mongo ... done
Creating easyavi       ... done
Creating mongo_express ... done
Inserting Default Password
Password Inserted Successfully!
```
Flag Usage
-
* Flags can also be used together

-r  This will remove the old easyavi container image and build a new one. Useful if you have made go/angular changes. 

*Note that for now if you make a UI change then from the easyavi-ui directory you must execute
```bash 
ng build --prod
```
You can then execute init.sh with the -r flag
```bash
./init.sh -r
```

-i  This will be used to insert an image collection for 20.1.6 into the DB. This requires that you have the controller-20.1.6.ova image in ./tmp which will be mapped to /root/images/ within the container. 
*Note that you must move this file yourself if you have not already downloaded it through easyavi.
```bash
./init.sh -i
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

